#include <X11/Xlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <fcntl.h>
#include <semaphore.h>

Display *display;
Window window;

enum corners { TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT };
enum corners corner = BOTTOM_RIGHT;

#define DISMISS_BUTTON Button2

static const unsigned int width = 28;

static const unsigned int color1 = 0xec0000;
static const unsigned int color2 = 0xecce00;

static const unsigned int duration = 500000;

static void die(const char *format, ...)
{
	va_list ap;
	va_start(ap, format);
	vfprintf(stderr, format, ap);
	fprintf(stderr, "\n");
	va_end(ap);
	exit(1);
}

void expire(int sig)
{
	XEvent event;
	event.type = ButtonPress;
	event.xbutton.button = DISMISS_BUTTON;
	XSendEvent(display, window, 0, 0, &event);
	XFlush(display);
}

int main(int argc, char *argv[])
{
	if (argc > 1)
	{
		sem_unlink("/batnotify");
		die("Usage: %s", argv[0]);
	}

	struct sigaction act_expire, act_ignore;

	act_expire.sa_handler = expire;
	act_expire.sa_flags = SA_RESTART;
	sigemptyset(&act_expire.sa_mask);

	act_ignore.sa_handler = SIG_IGN;
	act_ignore.sa_flags = 0;
	sigemptyset(&act_ignore.sa_mask);

	sigaction(SIGALRM, &act_expire, 0);
	sigaction(SIGTERM, &act_expire, 0);
	sigaction(SIGINT, &act_expire, 0);

	sigaction(SIGUSR1, &act_ignore, 0);
	sigaction(SIGUSR2, &act_ignore, 0);

	if (!(display = XOpenDisplay(0)))
		die("Cannot open display");

	int screen = DefaultScreen(display);
	Visual *visual = DefaultVisual(display, screen);
	Colormap colormap = DefaultColormap(display, screen);

	int screen_width = DisplayWidth(display, screen);
	int screen_height = DisplayHeight(display, screen);

	unsigned int color = color1;

	XSetWindowAttributes attributes;
	attributes.background_pixel = color;
	attributes.override_redirect = True;

	unsigned int x = 0;
	unsigned int y = 0;

	if (corner == TOP_RIGHT || corner == BOTTOM_RIGHT)
		x = screen_width - width;

	if (corner == BOTTOM_LEFT || corner == BOTTOM_RIGHT)
		y = screen_height - width;

	window = XCreateWindow(display, RootWindow(display, screen), x, y, width, width, 0, DefaultDepth(display, screen),
							CopyFromParent, visual, CWOverrideRedirect | CWBackPixel, &attributes);

	XSelectInput(display, window, ExposureMask | ButtonPress);
	XMapWindow(display, window);

	sem_t *mutex = sem_open("/batnotify", O_CREAT, 0644, 1);

	if (sem_trywait(mutex) != 0)
		exit(1);

	sigaction(SIGUSR1, &act_expire, 0);
	sigaction(SIGUSR2, &act_expire, 0);

	if (duration != 0)
		alarm(duration);

	for (;;)
	{
		XEvent event;
		usleep(duration);

		if (color == color1)
		{
			color = color2;
		}
		else
		{
			color = color1;
		}

		XSetWindowBackground(display, window, color);
		XClearWindow(display, window);
		XRaiseWindow(display, window);
		XFlush(display);

		if (XPending(display))
		{
			XNextEvent(display, &event);

			if (event.type == Expose)
			{
				XClearWindow(display, window);
			}
			else if (event.type == ButtonPress)
			{
				if (event.xbutton.button == DISMISS_BUTTON)
					break;
			}
		}
	}

	sem_post(mutex);
	sem_close(mutex);

	XCloseDisplay(display);

	return 0;
}
