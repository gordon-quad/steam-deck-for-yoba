#!/bin/bash
DIR=$(dirname "${0}")
export WINEPREFIX=$(realpath "${DIR}/prefix")
WINESLOT=$(cat "${DIR}/prefix/wineslot")
WINEVERSION=$(cat /etc/eselect/wine/wine.conf | grep ${WINESLOT} | head -1)
echo ${WINEVERSION}
export WINESERVER="/usr/lib/${WINEVERSION}/bin/wineserver"
export WINE="/usr/lib/${WINEVERSION}/bin/wine"
export WINEBOOT="/usr/lib/${WINEVERSION}/bin/wineboot"
export WINECFG="/usr/lib/${WINEVERSION}/bin/winecfg"
export WINE64="/usr/lib/${WINEVERSION}/bin/wine64"
export WINEARCH=win64
export WINEDEBUG="-all"
EXE=${1:-$(cat "${DIR}/exe")}
export WINEESYNC=1
#export WINE_SIMULATE_WRITECOPY=1
#export PROTON_USE_WINED3D11=1
#export WINEDLLOVERRIDES="dxgi,d3d9,d3d10,d3d10_1,d3d10core,d3d11=b;${WINEDLLOVERRIDES}"
cd "$(dirname "${EXE}")"
#${WINECFG}
#${WINE} control joy.cpl
trap '${WINESERVER} -k
      TIMEOUT=$((SECONDS+5))
      while [ ${SECONDS} -lt ${TIMEOUT} ];
      do
	      [ "$(pidof wineserver)" == "" ] && exit
	      sleep 0.2
      done
      ${WINESERVER} -k9' EXIT
${WINE} "$(basename "${EXE}")"
